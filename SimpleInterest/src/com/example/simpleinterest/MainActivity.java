package com.example.simpleinterest;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity implements OnClickListener {
	EditText amount, timeEdittext,ratio;
	Button addButton;
	TextView resultText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		amount = (EditText) findViewById(R.id.amount_edittext);
		timeEdittext = (EditText) findViewById(R.id.year_edittext);
		ratio = (EditText) findViewById(R.id.ratio_edittext);
		addButton = (Button) findViewById(R.id.add_button);
		resultText = (TextView) findViewById(R.id.resultText);
		addButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_button:
			if(amount.getText().toString().length() >0&&timeEdittext.getText().toString().length() >0&&ratio.getText().toString().length() >0){
				resultText.setVisibility(View.VISIBLE);
			double principle = Double.parseDouble(amount.getText().toString());
			double rateOfIntrest = Double.parseDouble(ratio.getText().toString());
			double time = Double.parseDouble(timeEdittext.getText().toString());
			double result = (principle * time * rateOfIntrest) / 100.0;
			resultText.setText("Rs. "+Math.rint(result)+"");
			}else{
				
				Toast.makeText(getApplicationContext(), "Fields should not be empty", Toast.LENGTH_SHORT).show();
			}
			break;

		default:
			break;
		}

	}

	
}
